# PonyTown-Datamining

> Finds interesting strings, code blocks, etc in Pony Town code

This project surveys both the Pony Town public and beta sites' assets and scripts in order to find interesting new features or sprites that haven't yet been mentioned to the public.Project commits and folders are labelled with the UTC date the samples were taken, and all assets are named with their respective rev tags. Only changed assets are posted.

## How to read this repo

Each commit represents an update to the game. `beta.pony.town` updates are labelled as follows:  
```
YYYY-MM-DD revision
```  
where `YYYY-MM-DD` is the date the update was pushed, and `revision` is the `bootstrap.js` rev tag used for cache busting.

`pony.town` updates are labelled as follows:  
```
XX.YY.ZZ [silent update YYY-MM-DD]
```  
where `XX.YY.ZZ` is the verison code the update launched with. If the update doesn't increment the version counter, the `silent update` text will be appended to the commit info, with the date the update was pushed on.

All commits are dated with the times the update was pushed (+/- 5 minutes).

Each `beta.pony.town` commit with substantial changes will have a comment attached detailing the exact changes as noted by myself and others. To view these comments, click on the update commit you wish to view, and the comment will be displayed at the bottom.

## Contributing

If you know anything about programming and VS Code, just add the two files (old and new) for comparison in Code. This should bring up a `diff`-like view in which you can compare the changes across the two files. From there, prepare a set of notes similar to the ones I provide, including a summary of the updates, as well as some synopsis of their meaning. Don't be too specific, but don't be too broad. Write documentation, not patch notes, but don't document every single `&&` to `||`.

Please write your notes before committing. This saves any headache if you forget to attach an asset file once you've noticed it's changed (speaking of, you may find a rev manifest in a part of the code - search for a large block of `images/`). Once you've committed, submit your notes as a comment on your commit.

## Sharing

Considering my rapport (or lack thereof) with the Pony Town staff as of late, I would prefer if this didn't get shared around *too* liberally. However, I'm also permanently banned, and it's not like they can do much more than that. Just be careful, and try not to get me in too much more trouble.

## License

All assets are copyright (c) Agamnentzar/Pony Town. I neither own nor claim any credit for the assets hosted in this repository. I am simply mining the contents of the scripts for useful information.
